<?php
namespace Cirrus;

require_once 'assets/CClassLoader.class.php';

class CModelLoader extends \Cirrus\CClassLoader {
	public function __class_loader($class_name) {

		$model_data = explode('\\', $class_name);
		if (!is_array($model_data) || count($model_data) < 2) return;
		$model_file = realpath(__DIR__ . '/../'.$model_data[0].'/Models/' . $model_data[1] . '.php');
		

		if (file_exists($model_file)) {
			include_once $model_file;
			return;
		}

		parent::__class_loader($class_name);
	}
}