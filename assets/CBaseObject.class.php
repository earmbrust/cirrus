<?php
namespace Cirrus;

/**
 *	\class 			CBaseObject
 *	\brief 			Base class for most other classes.
 *	\author 		Elden Armbrust
 *	
 *	CBaseObject is the base class for most other classes,
 *	and provides core functionality such as logging.
 */

abstract class CBaseObject {
	protected $Logger;		/*!< Reference to the current CSystem::Logger */
	protected $System;
	/**
	 *	\fn 				__construct($system, $parameters)
	 *	\brief				Class constructor. 
	 *	\param $system		The CSystem object to reference for system configurations.
	 *	\param $parameters	The parameters passed to the object, containing view-based configuration options, passed data, and other persisted information.
	 *
	 *	Connects the the local instance CBaseObject::Logger with the current CSystem::Logger instance.
	 */
	function __construct(&$system = null, &$parameters = null) {
		if ($system == null) die("Error ". __CLASS__." @ ".__LINE__);
		$this->Logger = $system->Logger;
		
		$this->System = $system;
		$this->Log("Loaded ".get_class($this));
	}
	
	function __destruct() {
		$this->Log(get_class($this)." shutting down.");
	}

	/**
	 *	\fn					Log($message)
	 *	\brief				Logs \a $message to the default (CLogger::DEBUG) log queue.
	 *	\param				$message The message to log.
	 *
	 *	CBaseObject::Log will log with the current timestamp, class, and provided message to the debug level logging queue.
	 */
	protected function Log($message) {
		if ($this->System != null) {
			$this->Logger->Log($message, $this, $this->System->Configuration->log_method);	
		} else {
			$this->Logger->Log($message, $this);
		}
	}
}