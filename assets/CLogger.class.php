<?php
namespace Cirrus;

/**
 *	\class			CLogger
 *	\brief			System-wide logging object
 *	\author			Elden Armbrust
 *
 */
class CLogger {

	const DEBUG = 1;
	const NOTICE = 2;
	const WARN = 3;
	const ERROR = 4;
	const NONE = -1;
	
	const CONSOLE = 0;
	const DISPLAY = 1;
	const SQLITE = 2;
	const CSVFILE = 3;
	
	
	private $system;
	private $configuration;
	
	function __construct(&$system, &$configuration) {
		$this->system = $system;
		$this->configuration = $configuration;
	}
	
	function __destruct() {
		//dumb idea... 
		//$this->Log('Destroying Logger...', $this);
	}
	
	public function Log($message, $caller=null, $level=CLogger::DEBUG) {
	
		//$debug_mode = true;
		//move to a unique class or struct
		
		$timestamp = microtime(true);
		$micro = sprintf("%06d", ($timestamp - floor($timestamp)) * 1000000);
		$local_time = new \DateTime( date('Y-m-d H:i:s.'.$micro, $timestamp));
		$log_time = $local_time->format("Y-m-d H:i:s.u");
		$log_data = array(
			'timestamp' => $timestamp,
			'message' => $message,
			'caller' => $caller,
			'log_time' => $log_time
		);
		
		
		switch ($level) {
			case CLogger::NONE:
				return;
				break;
			case CLogger::CONSOLE:
				//just run right over to DISPLAY for now...
			case CLogger::DISPLAY:
				if (is_array($message)) {
					echo var_export($message, true)."<br/>\n";
				} else {
					$caller_name = '';
					if (is_object($log_data['caller'])){
						$caller_name = get_class($log_data['caller']);
					} else if (is_string($log_data['caller'])) {
						$caller_name = $log_data['caller'];
					}
					echo '[',$log_data['log_time'],'] (',$caller_name,') - ',$message,"<br/>",PHP_EOL;
				}
				break;
			case CLogger::SQLITE:
				
				break;
			case CLogger::CSVFILE:
				
				break;
			default:
				break;
			
		}
	}
}