<?php 
namespace Cirrus;

require_once 'assets/CBaseObject.class.php';

class CClassLoader extends CBaseObject {
	private $__class;
	protected $System;
	
	function __construct(&$system = null, &$parameters = null) {
		parent::__construct($system, $parameters);
		$this->System = $system;
	}
	
	public function __class_loader($class_name) {

		$view_data = explode('\\', $class_name);
		
		// Handle unhandled dependencies
		$controller_file = realpath(__DIR__ . '/Controllers/'.$view_data[count($view_data)-1]. '.class.php');
		$view_file = realpath(__DIR__ . '/Views/'.$view_data[count($view_data)-1]. '.class.php');
		$model_file = realpath(__DIR__ . '/Models/'.$view_data[count($view_data)-1]. '.class.php');

		if (file_exists($controller_file)) {
			include_once $controller_file;
			return;
		} else if (file_exists($model_file)) {
			include_once $model_file;
			return;
		} else if (file_exists($view_file)) {
			include_once $view_file;
			return;
		}
		
		

	}
	
	public function Register() {
		$this->Log("Registering class loader...");
		spl_autoload_register(array($this, '__class_loader'));
	}
	
	protected function __loadclass($class) {
		spl_autoload_register(array($this, '__class_loader'));
		$this->Log("Creating instance of $class...");
		try {
			$ref = new \ReflectionClass($class); 
			$this->__class = $ref->newInstanceArgs(array(&$this->system, null));
			if (get_class($this->__class) == $class) {
				$this->Log("Class '$class' created successfully.");
			}
		} catch (\ReflectionException $re) {
			$this->Log("Failed to load class: $class.");
		}
		spl_autoload_unregister(array($this, '__class_loader'));
	}
	
	public function Unregister() {
		$this->Log("Unregistering class loader...");
	}
	
	function __destruct() {
		$this->Unregister();
		parent::__destruct();
	}
	
}