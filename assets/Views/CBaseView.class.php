<?php
namespace Cirrus;

require_once 'assets/Interfaces/IRenderable.interface.php';

require_once 'assets/CBaseObject.class.php';

require_once 'assets/HTML/Helper.class.php';

require_once 'assets/Libraries/Smarty/Smarty.class.php';

use HTML;

/**
 *	\class			CBaseView
 *	\brief			Base view class for app inheritance.
 *	\author			Elden Armbrust
 *
 */

class CBaseView extends \Cirrus\CBaseObject implements \Cirrus\IRenderable {
	public $parent;
	
	protected $template;
	private $_template_file;
	private $_title;
	protected $breadcrumbs = array();
	
	/**
	 *	\param $data		The data passed to this view for retrieval by the rendering subsytem.
	 *
	 *	Connects the the local instance CBaseView::Logger with the current CSystem::Logger instance.
	 */
	function __construct(CSystem &$system = null, &$parameters = null, $data=null) {
		parent::__construct($system, $parameters);
		$this->template = new \Smarty();
		$this->template->setTemplateDir($this->System->Configuration->app_name . '/Templates/');
	}
	
	function __destruct() {
		$this->Log("Destroying View...");
	}	

	function _render() {
		//Handle module render method dispatch
		$ModuleQueue = $this->System->getModuleQueue();
		$ModuleQueue->render();
		//$this->_render_doctype();
		$this->parseBreadCrumbs();
		$this->Render();
	}
	
	private function parseBreadcrumbs() {
		if (isset($_COOKIE['breadcrumbs'])) {
			$this->breadcrumbs = explode('*', addslashes($_COOKIE['breadcrumbs']));
			foreach($this->breadcrumbs as $key => $value) {
				$this->breadcrumbs[$key] = explode('^', $value);
			}
		}
	}
	
	public function generateBreadCrumbs() {
		$output = '<ul class="breadcrumbs">';
		$total = count($this->breadcrumbs);
		$pos = 0;
		foreach ($this->breadcrumbs as $value) {
			//var_dump($value);
			$pos++;
			if ($pos == $total) {
				$output .= '<li><span>'.$value[0].'</span></li>';
			} else {
				$output .= '<li><a href="'.$value[1].'">'.$value[0].'</a></li>';
			}
		}
		$output .= "</ul>";
		return $output;
	}
	
	protected function _render_doctype() {
		echo "<!DOCTYPE html>" , PHP_EOL;
	}
	
	function Render() {
		//Placeholder for inherited classes
	}
	
}