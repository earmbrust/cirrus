<?php
namespace Cirrus;

require_once 'assets/CBaseObject.class.php';
require_once 'assets/CLogger.class.php';
require_once 'assets/CViewLoader.class.php';
require_once 'assets/CModelLoader.class.php';


/**
 *	\class			CBaseController
 *	\brief			Base controller class for app inheritance.
 *	\author			Elden Armbrust
 *
 *	The CBaseController class provides low-level functionality for all
 *	classes extending from it.  Code common to all controllers should be
 *	located in this class, to promote the DRY methodology.
 */

class CBaseController extends CBaseObject{
	protected $system;				/*!< Reference to the current CSystem instance */
	protected $view;				/*!< Reference to the view that should be rendered */
	protected $PathIdentifier;		/*!< The PathIdentifier of the current route that this controller corresponds to */
	private $models = array();
	protected $_headers = array();
	
	function __construct(&$system = null, &$parameters = null) {
		parent::__construct($system, $parameters);
		$this->system = $system;
		$this->PathIdentifier = str_replace('Controller', '',get_class($this));
		$this->AddHeader("X-Powered-By", "Cirrus");
	}
	
	protected function AddHeader($key, $value) {
		$this->_headers[$key] = $value;
	}
	
	
	/**
	 *	\fn 				Build()
	 *	\brief				Builds the controller with model, view, and configuration parameters such as PathIdentifier.
	 *	\author 			Elden Armbrust
	 *
	 */
	public function Build() {
		if ($this->system != null){
			try {			
				$route = $this->system->getRoute();
				$this->system->consumeRoute($this->PathIdentifier);
				//TODO: Models shouldn't be loaded this way, but requested by the controller...
				$this->Log("Loading model for ".$this->PathIdentifier."...");
				$this->__loadmodel($this->PathIdentifier);
				$this->Log("Loading view for ".$this->PathIdentifier."...");
				$this->__loadview($this->PathIdentifier);			
			}
			catch (\ReflectionException $re) {
				$this->Log($re->getMessage());
			}
			
			catch (\Exception $e) {
				$this->Log($e->getMessage());
			}
		}
		
		foreach ($this->_headers as $field => $value) {
			$header = $field.": ".$value;
			header($header);
		}
		
		$this->view->_render();
	}
	
	
	function __destruct() {
		$this->Log("Destroying Controller...");
		unset($this->model);
		$this->model = null;
		unset($this->view);
		$this->view = null;
		parent::__destruct();
	}
	
	/**
	 *	\fn 				__loadview($view_name)
	 *	\brief				View loading method to be called from within the current instance.
	 *	\param $view_name	The name of the view to load, without the "View.php" suffix
	 *
	 *	Loads the view specified $view_name into the current CBaseController view.
	 */
	
	protected function __loadview($view_name){
		$this->Log("Autoloader for $view_name startup...");
		$view_loader = new CViewLoader($this->system);
		$view_loader->Register();
		$this->Log("Creating instance of $view_name...");
		try {
			$ref = new \ReflectionClass($view_name.'View'); 
			$this->view = $ref->newInstanceArgs(array(&$this->system, null));
			$this->view->parent = &$this;
			if (get_class($this->view) == $this->PathIdentifier.'View') {
				$this->Log("View '$view_name' created successfully.");
			}
		} catch (\ReflectionException $re) {
			$this->Log("Failed to load view for '$view_name'.");
		}
		$view_loader->Unregister();
	}
	
	
	/**
	 *	\fn 				__loadmodel($model_name)
	 *	\brief				Model loading method to be called from within the current instance.
	 *	\param $view_name	The name of the model to load, without the "Model.php" suffix
	 *
	 *	Loads the model specified by $model_name into the current CBaseController views model stack.
	 */
	protected function __loadmodel($model_name) {
		$this->Log("Autoloader for $model_name startup...");		
		$model_loader = new CModelLoader($this->system);
		$model_loader->Register();
		$this->Log("Creating instance of $model_name model...");
		try {
			$ref = new \ReflectionClass($model_name.'Model'); 
			$model = $ref->newInstanceArgs(array(&$this->system, null));
			$this->models[get_class($model)] = $model;
			if (get_class($model) == $this->PathIdentifier.'Model') {
				$this->Log("Model '$model_name' created successfully.");
			}
		} catch (\ReflectionException $re) {
			$this->Log("Failed to load model for '$model_name'.");
		}
		$model_loader->Unregister();
	}
	
    public function __toString()
    {
    	$string_buffer = array();
    	//var_dump($this->models);
    	foreach($this->models as $model) {
    		//echo $model->getData();
    		
    		$string_buffer[] = $model->getData();
    		//var_dump($model);
    	}
        return json_encode($string_buffer, true);
    }

}