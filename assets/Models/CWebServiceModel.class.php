<?php 
namespace Cirrus;

require_once 'assets/Models/CBaseModel.class.php';

class CWebServiceModel extends \Cirrus\CBaseModel {
	protected $remoteRoot;
	protected $requestRoute;
	protected $lastHTTPCode;
	
	public function setRemote($remote_uri) {
		$this->remoteRoot = $remote_uri;
	}
	
	public function setRoute($route) {
		$this->requestRoute = $route;
	}
	
	protected function __query() {
		$built_uri = $this->remoteRoot.$this->requestRoute;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $built_uri); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($ch);
		$this->data = $response;
		$this->lastHTTPCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
	}
	
}