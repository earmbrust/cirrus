<?php 
namespace Cirrus;

require_once 'assets/Models/CWebServiceModel.class.php';

class CJSONServiceModel extends \Cirrus\CWebServiceModel {
	protected function __query() {
		parent::__query();
		if (!empty($this->data) && is_string($this->data)) {
			$this->data = json_decode($this->data);
		}
		
	}
}