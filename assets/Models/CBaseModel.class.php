<?php
namespace Cirrus;

require_once 'assets/CBaseObject.class.php';

/**
 *	\class			CBaseModel
 *	\brief			Base model class for app inheritance.
 *	\author			Elden Armbrust
 *
 */

class CBaseModel extends \Cirrus\CBaseObject {

	protected $data;
	
	/**
	 *	\fn 				__construct($system, $parameters)
	 *	\brief				Class constructor. 
	 *	\param $system		The CSystem object to reference for system configurations.
	 *	\param $parameters	The parameters passed to the object, containing model-based configuration options, passed data, and other persisted information.
	 *	\author 			Elden Armbrust
	 *
	 *	Builds base class and creates local references to the system-wide configuration and parameters.
	 */
	function __construct(&$system = null, &$parameters = null, $data=null) {
		parent::__construct($system, $parameters);
	}
	
	function __destruct() {
		$this->Log("Destroying Model...");
		parent::__destruct();		
	}
	
	/**
	 *	\fn 				__query()
	 *	\brief				Internal service query method.
	 *	\author 			Elden Armbrust
	 *
	 *	Queries the specified resource for the requested asset.
	 */
	protected function __query() { }
	
	/**
	 *	\fn 				getData()
	 *	\brief				Returns the stored data retrieved from the data store.
	 *	\author 			Elden Armbrust
	 *
	 *	Returns the stored data retrieved from the data store. Queries the data store if the 'data' member variable has not been set.
	 *	This method should be left intact, while the query mechanism should set the 'data' member accordingly to follow the pattern set forth.
	 */	
	public function getData() {
		if (!$this->data) {
			$this->__query();
		}
		return $this->data;
	}
	
}