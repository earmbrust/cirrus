<?php 
namespace Cirrus;

interface IRenderable {
	function Render();
}