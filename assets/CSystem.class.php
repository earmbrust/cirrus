<?php 
namespace Cirrus;

require_once 'assets/CLogger.class.php';
require_once 'assets/CConfiguration.class.php';
require_once 'assets/CModuleQueue.class.php';
require_once 'assets/CControllerLoader.class.php';
require_once 'assets/CRoute.class.php';

/**
 *	\class			CSystem
 *	\brief			Bootstrapping system and core operator.
 *	\author			Elden Armbrust
 *
 *	CSystem is the bootstrapping mechanism, and puppet master to the
 *	framework.  It handle initial loading of the root controller,
 *	delegates application configuration loading, and more.
 */

class CSystem {
	public $Logger;
	public $Configuration;
	protected $Route;
	protected $PathIdentifier;
	private $RootController;
	private $ModuleQueue;
	
	public function &getModuleQueue() {
		return $this->ModuleQueue;
	}
	
	
	/**
	 *	\fn 				__construct($system, $parameters)
	 *	\brief				Class constructor. 
	 *
	 *	Bootstraps the Cirrus system and creates the root controller.
	 */
	function __construct() {
		$this->Configuration = new CConfiguration();
		if ($this->Configuration->error) {
			echo $this->Configuration->error;
			exit();
		}
		
		$this->Logger = new CLogger($this, $this->Configuration);
		
		$this->ModuleQueue = new CModuleQueue($this);
		
		$stuff = array();
		foreach ($this->Configuration->modules as $module_name => $options) {
			$this->ModuleQueue->Load($module_name, $options); 
		}
		
		$this->ModuleQueue->startup();
		$this->__get_route_data();
		if (!isset($this->Route) || empty($this->Route)) $this->Route = array('service' => $this->Configuration->default_controller);
		$this->Route = new CRoute($this->Route, $this);
		$this->__load_controller();
	}
	
	function __destruct() {
		if ($this->Logger) $this->Log('Shutting down main system...');
		unset($this->RootController);
		$this->RootController = null;		
		$this->ModuleQueue->shutdown();
		unset($this->Logger);
		$this->Logger = null;
	}
	

	
	private function __get_route_data() {
		$this->Log('Parsing route data...');
		$temp_route = $_SERVER['REQUEST_URI'];
		
		$this->Log('Setting systemwide route data...');
		$temp_route = str_replace($this->Configuration->web_root, '', $temp_route);
		
		if (strlen($temp_route) > 0){
			$temp_route = explode('/', trim($temp_route, "/"));
			$this->Route = $temp_route;
		}
		
	}

	/**
	 *	\fn 				getRoute()
	 *	\brief				Retrieves the current route.
	 *
	 */
	public function getRoute(){
		return $this->Route;
	}

	/**
	 *	\fn 				function consumeRoute($name)
	 *	\brief				Retrieves the current route.
	 *	\param	$name		The name of the route to consume.
	 *
	 *	Consumes a route if the given name matches the currently active route.
	 *
	 */
	public function consumeRoute($name){
		//now deprecated, we no longer need to force a route consumption.
		return true;
	}
	
	private function Log($message) {
		$this->Logger->Log($message, $this, $this->Configuration->log_method);	
	}
	
	
	private function __load_controller() {
		$app_dir = $this->Configuration->app_name;
		$controller_name = $this->Route->service;

		$controller_loader = new CControllerLoader($this);
		$controller_loader->Register();
		$this->Log("Creating instance of $controller_name...");
		try {
			$ref = new \ReflectionClass($app_dir.'\\'.$controller_name.'Controller'); 
			$this->RootController = $ref->newInstanceArgs(array(&$this, null));			
		} catch (\ReflectionException $re) {
			$this->Log($re->getMessage());
			$this->Log("Failed to load controller for '$controller_name'.");
		}
		$controller_loader->Unregister();
		
		if (get_class($this->RootController) == $app_dir.'\\'.$this->Route->service.'Controller') {
			$this->RootController->Build();
			$this->Log("View '$controller_name' created successfully.");
		}		
	}	
}