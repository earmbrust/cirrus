<?php
namespace HTML;

class Helper {
	private $short_tags = array('link', 'hr', 'br', 'meta');


	private $tag;
	public $parent = null;
	public $content;
	private $attributes;
	private $children = array();
	private $rendered = false;
	public $tabbing = 0;
	private $self_closing = false;
	
	
	public function __call($tag, $arguments) {
		$child = new Helper($tag);
		foreach ($arguments as $arg) {
			if (is_string($arg)) {
				$json = json_decode($arg);
				if ($json != null) {
					foreach ($json as $key => $value) {
						$child->$key = $value;
					}
				} else {
					$child->content = $arg;
				}
			}
		}
		
		$child->parent = &$this;
		$child->tabbing = $this->tabbing + 1;
		if (in_array($tag, $this->short_tags)) {
			$this->self_closing = true;
		}
		
		array_push($this->children, $child);
		return $child;
	}
	
	public static function __callStatic($tag, $arguments) {
		$child = new Helper($tag);
		foreach ($arguments as $arg) {
			if (is_string($arg)) {
				$json = json_decode($arg);
				if ($json != null) {
					foreach ($json as $key => $value) {
						$child->$key = $value;
					}
				} else {
					$child->content = $arg;
				}
			}
		}
		return $child;
	}
	
	public function __construct($tag, $arguments = null) {
		$this->tag = $tag;
		if (!isset($arguments) || empty($arguments)) return;
		foreach ($arguments as $arg) {
			if (is_string($arg)) {
				$json = json_decode($arg);
				if ($json != null) {
					foreach ($json as $key => $value) {
						$this->$key = $value;
					}
				} else {
					$this->content = $arg;
				}
			}
		}
	}
	
	
	public function __destruct() {
		if ($this->rendered == false) {
			$this->render();
		}
	}
	
	public function render() {
		if ($this->parent != null) {
			$this->parent->render();
		} else if ($this->rendered == false) {
			$this->__render();
			$this->rendered = true;
		}
	}
	
	public function __render() {
		
		$indent = "";
		for ($tab = 0; $tab < $this->tabbing; $tab++) {
			$indent .= "\t";
		}
		
		echo $indent, '<',$this->tag;
		if (is_array($this->attributes)) {
			foreach ($this->attributes as $attrib => $value) {
				switch ($attrib) {
					case 'content':
						if (is_string($value)) {
							$this->content = $value;
						}
						break;
					case 'selfclosing':
						if (is_string($value) && $value == 'true') {
							$this->self_closing = true;
						}
						break;
					default:
						echo ' ',$attrib,'="',$value,'"';
						break;
				}
				
			}
		}
		
		
		if ($this->self_closing === true) {
			echo ' />' , PHP_EOL;
		} else {
			echo '>' , PHP_EOL;
		}
		foreach ($this->children as $child) {
			$child->__render();
		}
		
		
		if (strlen($this->content) > 0 && $this->self_closing !== true) echo $this->content;
		
		
		if ($this->self_closing !== true) echo $indent, '</',$this->tag,'>' , PHP_EOL;
	}
	
	public function __get($var) {
		switch ($var) {
			case 'tabbing':
				return $this->tabbing;
			default:
				if (isset($this->attributes[$var])) {
					return $this->attributes[$var];
				}
				break;
		}
		return null;
	}
	
	public function __set($var, $value) {
		switch ($var) {
		case 'tabbing':
			$this->tabbing = $value;
			break;
		default:
			$this->attributes[$var] = $value;
			break;
		}
	}
	
	public function add(&$child) {
		array_push($this->children, $child);
		return $this;
	}

}