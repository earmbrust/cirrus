<?php 
namespace Cirrus;

/**
 *	\class			MBaseModule
 *	\brief			Base module class for Cirrus Module inheritance.
 *	\author			Elden Armbrust
 *
 */

class MBaseModule {
	protected $options;
	
	function __construct($options = null) {
		$this->options = $options;
	}
	
	public function startup() {
		
	}

	public function render() {
		//var_dump($this->options);
	}
	
	
	
	public function shutdown() {
		
	}
}