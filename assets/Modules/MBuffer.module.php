<?php 
namespace Cirrus;

require_once 'assets/Modules/MBaseModule.module.php';

/**
 *	\class			MBuffer
 *	\brief			Buffer module for buffering and compressing view output.
 *	\author			Elden Armbrust
 *
 *	The MBuffer module provides buffered content serving through
 *	the standard PHP output buffering mechanisms.  The gzip compression
 *	method is supported, but should only be used on servers that do not use
 *	request based server-side compression.
 */

class MBuffer extends MBaseModule {
	private $active = false;
	
	public function startup() {
		parent::startup();
		if (headers_sent() || !ob_start("ob_gzhandler")) ob_start();

		$this->active = true;
	}
	
	
	public function shutdown() {
		if ($this->options["trim_whitespace"] === true) {
			$output = trim(ob_get_contents());
			ob_end_clean();
			echo $output;
		} else {
			ob_end_flush();
		}
		parent::shutdown();
	}
}