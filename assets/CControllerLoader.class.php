<?php
namespace Cirrus;

require_once 'assets/CClassLoader.class.php';

class CControllerLoader extends \Cirrus\CClassLoader {
	public function __class_loader($class_name) {
		$controller_data = explode('\\', $class_name);
		if (!is_array($controller_data) || count($controller_data) < 2) return;
		$controller_file = __DIR__ . '/../'.$controller_data[0].'/Controllers/' . $controller_data[1] . '.php';
		

		if (file_exists($controller_file)) {
			include_once $controller_file;
			return;
		}
		
		parent::__class_loader($class_name);		
	}
}