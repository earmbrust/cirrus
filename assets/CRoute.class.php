<?php
namespace Cirrus;

require_once 'assets/CBaseObject.class.php';

class CRoute extends \Cirrus\CBaseObject {
	private $_service;
	private $_action;
	private $_object;

	public function __construct($route_data, &$system = null, &$parameters = null){
		parent::__construct($system, $parameters);
		if (is_array($route_data)) {
			if (isset($route_data['service'])){
				$this->_service = $route_data['service'];
			} else if (isset($route_data[0])){
				$this->_service = $route_data[0];
			}
			
			if (isset($route_data['action'])){
				$this->_action = $route_data['action'];
			} else if (isset($route_data[1])){
				$this->_action = $route_data[1];
			}
			
			if (isset($route_data['object'])){
				$this->_object = $route_data['object'];
			} else if (isset($route_data[2])){
				$this->_object = $route_data[2];
			}
		}		
	}
	
	public function __set($name, $value)
    {
        switch($name) {
        	case 'service':
        		$this->_service = $value;
        		break;
        	case 'action':
        		$this->_action = $value;
        		break;
        	case 'object':
        		$this->_object = $value;
        		break;
        	default:
        		break;
        }
    }

    public function __get($name)
    {
		switch($name) {
			case 'service':
        		return $this->_service;
        		break;
			case 'action':
        		return $this->_action;
        		break;
        	case 'object':
        		return $this->_object;
        		break;
        	default:
        		break;
        }
    }
}