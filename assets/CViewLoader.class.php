<?php
namespace Cirrus;

require_once 'assets/CClassLoader.class.php';

class CViewLoader extends \Cirrus\CClassLoader {
	public function __class_loader($class_name) {

		$view_data = explode('\\', $class_name);
		if (!is_array($view_data) || count($view_data) < 2) return;
		$view_file = realpath(__DIR__ . '/../'.$view_data[0].'/Views/' . $view_data[1] . '.php');
		

		if (file_exists($view_file)) {
			include_once $view_file;
			return;
		}

		parent::__class_loader($class_name);
	}
	
}