<?php
namespace Cirrus;

require_once 'assets/CLogger.class.php';

/**
 *	\class			CConfiguration
 *	\brief			Lightweight object class for app configuration data.
 *	\author			Elden Armbrust
 *
 *	The CConfiguration class provides simple configuration loading
 *	and access.
 */

class CConfiguration {

	private $data = array();
	
	function __construct() {
		$this->__loadconfiguration();
	}
	
	private function __loadconfiguration() {
		if (!file_exists('manifest.json')) {
			$this->error = "Unable to load configuration.";
			return;
		}
		//TODO: Replace for a less wonky loader...
		$config_data = file_get_contents('manifest.json');
		$obj = json_decode($config_data, true);
		foreach ($obj as $key => $value) {
			
			$this->$key = $value;
		}
	}
	
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }
}