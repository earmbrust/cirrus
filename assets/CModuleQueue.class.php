<?php
namespace Cirrus;

require_once 'assets/CBaseObject.class.php';
require_once 'assets/CLogger.class.php';

/**
 *	\class			CModuleQueue
 *	\brief			Module process managing, instantiating, and event triggering class.
 *	\author			Elden Armbrust
 *
 *	CModuleQueue is the module manager and event triggering mechanism for all modules referenced within
 *	an app.
 */

class CModuleQueue extends CBaseObject {
	protected $Logger;
	private $Modules;
	
	
	protected function __loadmodule($module_name, $options) {
		//@TODO: get value from config/system...
		$root = "assets/Modules/";
		if (file_exists($root."M".$module_name.".module.php")) {
			include_once $root."M".$module_name.".module.php";
			
			$ref = new \ReflectionClass('Cirrus\\'."M".$module_name);
			$this->Modules[] = $ref->newInstanceArgs(array($options));
			$this->Log("Loaded '$module_name' module...");
		} else {
			$this->Log("Unable to locate '$module_name' module, skipping.");
		}		
	}
	
	public function Load($module_name, $options = array()) {
		$this->__loadmodule($module_name, $options);
	}
	
	public function startup() {
		if (is_array($this->Modules) && count($this->Modules) > 0) {
			foreach($this->Modules as $module) {
				$module->startup();
			}
		}
	}
	
	public function render() {
		if (is_array($this->Modules) && count($this->Modules) > 0) {
			foreach($this->Modules as $module) {
				$module->render();
			}
		}
	}
	
	public function shutdown() {
		if (is_array($this->Modules) && count($this->Modules) > 0) {
			foreach($this->Modules as $module) {
				$module->shutdown();
			}
		}
	}
	
	
}