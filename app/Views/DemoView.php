<?php
namespace app;

class DemoView extends \Cirrus\CBaseView {
	protected $template;
	
	public function Render() {
		$this->template->assign('breadcrumbs', $this->generateBreadCrumbs());
		$this->template->display($this->System->getRoute()->service . '.tpl');
	}
}