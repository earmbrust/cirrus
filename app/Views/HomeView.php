<?php
namespace app;

class HomeView extends \Cirrus\CBaseView {
	protected $template;
	
	public function Render() {
		$this->template->assign('breadcrumbs', $this->generateBreadCrumbs());
		$this->template->display($this->System->getRoute()->service . '.tpl');
	}
}