<!DOCTYPE html>
<html>
<head>
	<title>Demo App</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" type="image/png" href="favicon.png" />
	<link rel="stylesheet" type="text/css" href="css/foundation.css">
	<link rel="stylesheet" type="text/css" href="css/ui.css">
	<link rel="stylesheet" type="text/css" href="css/forms.css">
	<link rel="stylesheet" type="text/css" href="css/typography.css">
	<link rel="stylesheet" type="text/css" href="css/orbit.css">
	<link rel="stylesheet" type="text/css" href="css/reveal.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/core.css">
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print">
</head>
<body>		
			<div class="container">
				<div class="row">
					<div id="top" class="twelve columns">
						<!-- Top/Logo area -->
					</div>
				</div>
		<div class="row">
	<ul class="menu twelve columns">
		<li>
			<a href="/cirrus/">
Home			</a>
		</li>
		<li>
			<a href="#">
Tutorials			</a>
			<ul class="submenu">
				<li>
					<a href="?t=test">
Sub Nav Link					</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="/cirrus/">
Test			</a>
		</li>
	</ul>
</div>
				<div class="row">
					<div id="breadcrumbs" class="gray twelve columns breadcrumbs">{$breadcrumbs}</div>
				</div>
				
				
				<div class="row">
					<dl class="tabs eight columns">
					  <dd><a href="#simple1" class="active">Simple Tab 1</a></dd>
					  <dd><a href="#simple2">Simple Tab 2</a></dd>
					  <dd><a href="#simple3">Simple Tab 3</a></dd>
					</dl>
					
					<ul class="tabs-content eight columns">
						<li class="active" id="simple1Tab">
							<div class="eight columns">
								<!-- Main content area -->
								
					          	
							</div>
			
						</li>
						<li id="simple2Tab"><div class="eight columns">This is simple tab 2's content. Now you see it!</div></li>
						<li id="simple3Tab"><div class="eight columns">This is simple tab 3's content. It's, you know...okay.</div></li>
					</ul>
		
		
					<div class="four columns">
						<!-- Widget content area -->
						<div class="panel">
							<h5>Panel Example</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis iaculis erat. Vivamus felis justo, hendrerit sit amet volutpat ut, rutrum eu mi. Integer eget leo odio. Praesent fermentum tincidunt mi, nec lobortis neque consequat sed. Pellentesque id leo viverra est cursus aliquam ac ac felis. Nullam eu nunc tellus. Integer ut aliquam nisi. Morbi ultrices fringilla sollicitudin. Nulla facilisi. Fusce congue tempus mi. Etiam non enim sit amet nisl vehicula tristique in a dolor. Proin lobortis consectetur ipsum auctor molestie.</p>
			
							<p>Nam lacus mauris, placerat sit amet congue sit amet, vestibulum sed purus. Vestibulum accumsan mollis mi id posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc venenatis lectus ornare sapien fringilla at imperdiet velit auctor. Curabitur dui tellus, tristique sed consectetur at, pulvinar sit amet libero. Sed interdum venenatis eros, quis volutpat lorem sagittis et. Sed posuere quam sit amet tellus consectetur auctor. Maecenas at elit ipsum, at posuere urna. Donec accumsan ullamcorper libero a tincidunt.</p>
						</div>
					</div>
				</div>
				<div class="row">	
					<div id="footer" class="grid_15">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis iaculis erat. Vivamus felis justo, hendrerit sit amet volutpat ut, rutrum eu mi. Integer eget leo odio. Praesent fermentum tincidunt mi, nec lobortis neque consequat sed. Pellentesque id leo viverra est cursus aliquam ac ac felis. Nullam eu nunc tellus. Integer ut aliquam nisi. Morbi ultrices fringilla sollicitudin. Nulla facilisi. Fusce congue tempus mi. Etiam non enim sit amet nisl vehicula tristique in a dolor. Proin lobortis consectetur ipsum auctor molestie.
			
			Nam lacus mauris, placerat sit amet congue sit amet, vestibulum sed purus. Vestibulum accumsan mollis mi id posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc venenatis lectus ornare sapien fringilla at imperdiet velit auctor. Curabitur dui tellus, tristique sed consectetur at, pulvinar sit amet libero. Sed interdum venenatis eros, quis volutpat lorem sagittis et. Sed posuere quam sit amet tellus consectetur auctor. Maecenas at elit ipsum, at posuere urna. Donec accumsan ullamcorper libero a tincidunt.
						
					</div>
				</div>
				<span class="clear"></span>
			</div>
		
		
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.breadcrumbs.js" charset="utf-8"></script>
<script type="text/javascript" src="js/foundation.js" charset="utf-8"></script>
<script type="text/javascript" src="js/modernizr.foundation.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.customforms.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.orbit-1.4.0.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.tooltips.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.reveal.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/app.js" charset="utf-8"></script>
<script type="text/javascript" src="js/core.js" charset="utf-8"></script>
</body>
</html>