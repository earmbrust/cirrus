<?php 
namespace app;
require_once 'assets/Models/CJSONServiceModel.class.php';

class HomeModel extends \Cirrus\CJSONServiceModel {
	function __construct(&$system = null, &$parameters = null) {
		parent::__construct($system, $parameters);
		$this->setRemote('http://testing.molecularstudios.com/webapp.json');
	}
}